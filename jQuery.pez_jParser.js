/**
* Created with PyCharm.
* User: pZhemchugov
* Date: 10.06.13
* v.1.0
*
* options:	.jParse (json, {options});
* close: true/false	->	Наличие кнопки,
* closeClass		->	ClassName кнопки,
* closeText			->  Текст кнопки.
*
* ulClass			->	Класс родитнльского <ul/>
* liClass			->	Класс <li/> - объекта
* toggleClass		->	Класс у открытого объекта
* keyClass			->	Класс Ключа (<span/> или <a/> - в зависимости ключ или объект )
*/
(function($){
	jQuery.fn.jParse = function(json, options){
		options = $.extend({
			//Базовые настройки
			close		:	false,
			closeClass	:	'jParseClose',
			closeText	:	'Свернуть',
			ulClass 	: 	'jParse',
			liClass		:	'jParseObj',
			toggleClass	:	'jParseOpen',
			keyClass	:	'jParseKey'
		}, options);
		var
		result = '',
		parse = function(){
			var _this = $(this),
				_json = json;
			if(!json)
				_json = JSON.parse(_this.html());

			// здесь переменная this содержит DOM элемент к которому применяется плагин
			result = '<ul class="'+ options.ulClass +'">';
			$.each(_json, getObject);
			result += '</ul>';
			$(this).empty().append(result);
			$('ul.'+ options.ulClass+' a', $(this)).bind('click',function(){
				$(this).parent().toggleClass(options.toggleClass);
				return false;
			});
			if(options.close){
				_this.append('<a href="" class="'+ options.closeClass +'">'+ options.closeText +'</a>');
				$('a.'+options.closeClass, _this).bind('click',function(){
					$('ul.'+ options.ulClass+' li.' + options.liClass, _this).removeClass(options.toggleClass);
					return false;
				});
			}
		},
		getObject = function(key, value){
			if(value instanceof Object){
				var brackets = ( value.length == undefined ) ? "{}"	:	"[" + value.length + "]";
				result += '<li class="'+ options.liClass+'"><a href="" class="'+ options.keyClass+'">' + key + brackets + '</a><ul>';
				$.each(value, getObject);
				result += '</ul></li>';
			}else{
				result +=  "<li><span class='"+ options.keyClass+"'>" + key  + "</span>  :  " +  value +'</li>';
			}
		};
		return this.each(parse);
	};
})(jQuery);