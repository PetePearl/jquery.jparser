jQuery.pez_jParser.js
Плагин предназначен для отображения jSon массива на web странице в виде а-ля fireBug.

Использование плагина:
$('div').jParse(JSON);   //вызов функции со стандартными настройками,
 JSON - строка Json
 выборка $('div') - элемент в который будт выведем JSON массив.


$('div').jParse(JSON,{   //вызов функции с кастомными настройками
  close       :  false,         //  Наличие кнопки            [true/false],
  closeClass  :  'jParseClose', //  ClassName кнопки,         [строка]
  closeText   :  'Свернуть',    //  Текст кнопки.             [строка]
  ulClass     :   'jParse',     //  Класс родитнльского <ul/> [строка]
  liClass     :  'jParseObj',   //  Класс <li/> - объекта     [строка]
  toggleClass :  'jParseOpen',  //  Класс у открытого объекта [строка]
  keyClass    :  'jParseKey'    //  Класс ключа               [строка]
});